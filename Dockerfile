FROM ubuntu:14.04
MAINTAINER tym

RUN apt-get update
RUN apt-get install -y python-pip
RUN pip install shadowsocks

RUN echo '{' >> ssconfig.json
RUN echo '"server": "::",' >> ssconfig.json
RUN echo '"server_port": 8388,' >> ssconfig.json
RUN echo '"local_port": 1080,' >> ssconfig.json
RUN echo '"timeout": 600,' >> ssconfig.json
RUN echo '"method": "aes-256-cfb"' >> ssconfig.json
RUN echo '}' >> ssconfig.json

ENV PASSWORD your_shadowsocks_password
EXPOSE 8388

ENTRYPOINT ['sh', '-c', 'ssserver -c ssconfig.json -k $PASSWORD']
